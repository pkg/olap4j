Source: olap4j
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Damien Raude-Morvan <drazzib@debian.org>
Build-Depends: ant, cdbs, debhelper (>= 10), default-jdk, maven-repo-helper
Build-Depends-Indep: ant-contrib,
                     ant-optional,
                     default-jdk-doc,
                     javacc,
                     junit,
                     junit-doc,
                     libcommons-dbcp-java,
                     libeigenbase-farrago-java,
                     libmondrian-java,
                     libxerces2-java
Standards-Version: 4.1.2
Vcs-Git: https://anonscm.debian.org/git/pkg-java/olap4j.git
Vcs-Browser: https://anonscm.debian.org/git/pkg-java/olap4j.git
Homepage: http://www.olap4j.org/

Package: libolap4j-java
Architecture: all
Depends: libxerces2-java,
         ${misc:Depends}
Suggests: libcommons-dbcp-java
Description: unified Java API to access an OLAP server
 olap4j is a Java library to access a OLAP server with an unified API :
 you may switch your OLAP server to another implementation with ease.
 .
 It can be compared to JDBC for SQL servers.
 .
 This project is supported by many libre software project in this area :
  * Jasperreports
  * Mondrian
  * Pentaho

Package: libolap4j-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Recommends: default-jdk-doc, junit-doc
Suggests: libolap4j-java
Description: unified Java API to access an OLAP server - documentation
 olap4j is a Java library to access a OLAP server with an unified API :
 you may switch your OLAP server to another implementation with ease.
 .
 It can be compared to JDBC for SQL servers.
 .
 This project is supported by many libre software project in this area :
  * Jasperreports
  * Mondrian
  * Pentaho
 .
 This package contains Javadoc API of olap4j package.
