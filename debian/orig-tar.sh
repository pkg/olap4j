#!/bin/sh -e

# Extract and repack upstream source (zip in upstream zip package)
# $2 = version
# $3 = file

PACKAGE=$(dpkg-parsechangelog -S Source)
VERSION=$2
DIR=${PACKAGE}-${VERSION}
TAR=../${PACKAGE}_${VERSION}.orig.tar.xz

# Repack upstream source to tar.xz
mkdir $DIR
(
cd $DIR
unzip -j ../$3 olap4j-*/olap4j-*-src.zip
unzip -d . olap4j-*-src.zip
mv olap4j-*/* .
rm olap4j-*-src.zip
)
rm $3

XZ_OPT=--best tar cJvf $TAR -X debian/orig-tar.exclude $DIR
rm -rf $DIR
